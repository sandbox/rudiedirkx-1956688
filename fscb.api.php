<?php

/**
 * Implements hook_fscb_menu_alter().
 *
 * Allows for altering the menu items created in _fscb_menu_groups(). This does
 * not include the existing menu items form hook_menu(). They are always overwritten
 * but can be altered later through hook_menu_alter().
 *
 * @see _fscb_menu_groups().
 */
function hook_fscb_menu_alter(&$fscb_items) {
  // Create a very special access callback for node/%node/details.
  // @see fscb_access_callback().
  $fscb_items['node/%node/details']['access callback'] = 'MODULE_fscb_node_details_access_callback';

  /**
  function MODULE_fscb_node_details_access_callback($identifiers) {
    global $user;
    return $user->uid == 1;
  }
  /**/
}

/**
 * Implements hook_fscb_suppress().
 *
 * Returning TRUE will suppress FSCB's split and do nothing more.
 *
 * @see _fscb_form_alter().
 */
function hook_fscb_suppress($form, $form_state) {
  $entity_type = $form['#entity_type'];
  $bundle = $form['#bundle'];
  $field_group = isset($form_state['fscb_group']) ? $form_state['fscb_group']->group_name : '';

  switch ($entity_type) {
    // Don't split new nodes for admins.
    case 'node':
      global $user;
      return $user->uid == 1 && empty($form['#node']->nid);

    // Don't split new users.
    case 'user':
      return $form['#form_id'] == 'user_register_form';
  }
}
